﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RadixDesafioSensores.Models;


namespace RadixDesafioSensores.Controllers
{
    public class EventosController : Controller
    {
        private static List<EventoModel> eventos = new List<EventoModel>();

        [HttpGet]
        public string Get()
        {
            HttpClient http = new HttpClient();
            //Existe uma pasta chamada 'API' na raiz do projeto onde é possível encontrar o código que gera os dados da API. 
            var data = http.GetAsync("https://randomapi.com/api/1b4050e14ab0a303bfd2eb5fd0cf5905").Result.Content.ReadAsStringAsync().Result;

            var eventoResult = JsonConvert.DeserializeObject<EventoModel>(data.Substring(0, data.IndexOf(']')).Substring(12));

            var resultado = new
            {
                timestamp = eventoResult.Timestamp,
                tag = eventoResult.Tag,
                valor = eventoResult.Valor
            };

            eventos.Add(new EventoModel(resultado.timestamp, resultado.tag, Convert.ToInt32(resultado.valor)));

            return data;
        }

        public IActionResult Index()
        {
            return View(eventos);
        }

        public IActionResult Sobre()
        {
            return View();
        }

        public string GerarDados()
        {
            List<string> regiao = new List<string>( new string[] { "sul", "sudeste", "norte", "nordeste" } );
            var sensor = new List<string>(new string[] { "sensor01", "sensor02" });
            var random = new Random();
            StringBuilder str = new StringBuilder();

            str.Append("brasil.");
            str.Append(regiao[random.Next(regiao.Count)]);
            str.Append("." + sensor[random.Next(sensor.Count)]);

            var resultado = new
            {
                timestamp = DateTimeOffset.UtcNow.ToUnixTimeSeconds(),
                tag = Convert.ToString(str),
                valor = Convert.ToString(random.Next(0, 20))
            };

            eventos.Add(new EventoModel(resultado.timestamp, resultado.tag, Convert.ToInt32(resultado.valor)));
            System.Threading.Thread.Sleep(TimeSpan.FromSeconds(0.5));

            return JsonConvert.SerializeObject(resultado);
        }

        public void LimparDados()
        {
            eventos.Clear();
        }

        public string SudesteDados()
        {
            var sudeste = new
            {
                total = eventos.Where(x => x.Tag.Contains("sudeste")).Select(x => x.Valor > 0).Count(),
                sensor01 = eventos.Where(x => x.Tag.Contains("sudeste") && x.Tag.Contains("sensor01")).Select(x => x.Valor > 0).Count(),
                sensor02 = eventos.Where(x => x.Tag.Contains("sudeste") && x.Tag.Contains("sensor02")).Select(x => x.Valor > 0).Count()
            };
            return JsonConvert.SerializeObject(sudeste);
        }

        public string SulDados()
        {
            var sudeste = new
            {
                total = eventos.Where(x => x.Tag.Contains("sul")).Select(x => x.Valor > 0).Count(),
                sensor01 = eventos.Where(x => x.Tag.Contains("sul") && x.Tag.Contains("sensor01")).Select(x => x.Valor > 0).Count(),
                sensor02 = eventos.Where(x => x.Tag.Contains("sul") && x.Tag.Contains("sensor02")).Select(x => x.Valor > 0).Count()
            };
            return JsonConvert.SerializeObject(sudeste);
        }

        public string NorteDados()
        {
            var sudeste = new
            {
                total = eventos.Where(x => x.Tag.Contains("norte")).Select(x => x.Valor > 0).Count(),
                sensor01 = eventos.Where(x => x.Tag.Contains("norte") && x.Tag.Contains("sensor01")).Select(x => x.Valor > 0).Count(),
                sensor02 = eventos.Where(x => x.Tag.Contains("norte") && x.Tag.Contains("sensor02")).Select(x => x.Valor > 0).Count()
            };
            return JsonConvert.SerializeObject(sudeste);
        }

        public string NordesteDados()
        {
            var sudeste = new
            {
                total = eventos.Where(x => x.Tag.Contains("nordeste")).Select(x => x.Valor > 0).Count(),
                sensor01 = eventos.Where(x => x.Tag.Contains("nordeste") && x.Tag.Contains("sensor01")).Select(x => x.Valor > 0).Count(),
                sensor02 = eventos.Where(x => x.Tag.Contains("nordeste") && x.Tag.Contains("sensor02")).Select(x => x.Valor > 0).Count()
            };
            return JsonConvert.SerializeObject(sudeste);
        }
    }
}