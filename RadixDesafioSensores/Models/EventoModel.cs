﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RadixDesafioSensores.Models
{
    public class EventoModel
    {
        [JsonProperty("timestamp")]
        public Int64 Timestamp { get; set; }

        [JsonProperty("valor")]
        public int Valor { get; set; }

        [JsonProperty("tag")]
        public string Tag { get; set; }

        public EventoModel(Int64 horario, string tag, int valor)
        {
            this.Timestamp = horario;
            this.Tag = tag;
            this.Valor = valor;
        }
    }
}
