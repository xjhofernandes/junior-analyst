# Sistema de consumo de dados de radares no Brasil: Ultrapassagem de velocidade

## Como o sistema funciona?

O sistema irá consumir o número de ultrapassagens de velocidade no trânsito por segundo, captados por radares (sensores) de trânsitos nas regiões Sul, Sudeste, Norte e Nordeste.

Os dados serão adicionados em uma tabela dinâmica, com os avisos se foram ou não processados. E, junto a isso, montar um gráfico que mostre a quantidade de multas feitas por dia em suas respectivas regiões (Sul, Sudeste, Norte e Nordeste). 

Com os dados obtidos é possível construir um gráfico que permita comparar o número de ultrapassagens de velocidade computadas pelos radares de trânsitos nas regiões Sul, Sudeste, Norte e Nordeste partindo da tabela dinâmica. Com isso, decorrendo o tempo, é possível formulação de gráficos que comparem esses dados entre as regiões por dia, mês e ano. O comparativo final para o ano determinado (ex: 2019) seria um gráfico que mostrasse o número de ultrapassagens cometidos por dia/ano.


Na página inicial, canto superior esquerdo, há o botão "Gerar Dados". Esse botão permite adicionar os números referentes as ultrapassagens de velocidade em cada região do Brasil por segundo. Tais dados usados são genéricos, gerados aleatoriamente partindo de um algoritimo criado para o desafio. 

O sistema para ter a aplicação na realidade teria seus sensores como os dados obtidos e processados pelos radares de trânsito. Na montagem do desafio foi empregado apenas dois sensores como solicitado no desafio. Na prática, o número de radares seria bem grande. Sendo assim, seria necessário um banco de dados rápido e de fácil acesso aos dados (O caso do mongodb, que funcionaria bem pelo seu desempenho com a manipulação de uma grande quantidade de dados).

Foi adicionada uma API que gera os dados pedidos para o projeto, que se limita a 500 chamadas por dias (Por conta de ser gratuíta). Ela foi escrita em Javascript e vou disponibilizar o código da mesma.   

## Tecnologias utilizadas

 *  .Net Core 2.1
 *  Javascript/Html/CSS
 *  Jquery Utilizado para chamadas de funções AJAX
 *  Postman para testes 
 *  Random API, api gratuíta para consumo de dados. URL: https://randomapi.com/
 
 
 ## Preview do projeto
 [![Exemplo do sistema](https://i.imgur.com/pT4pYUd.jpg "Exemplo do sistema")](https://i.imgur.com/pT4pYUd.jpg "Exemplo do sistema")